package lectures;

import beans.Person;
import java.util.List;
import java.util.stream.IntStream;
import mockdata.MockData;
import org.junit.Test;

public class Lecture2 {

  @Test
  public void range() throws Exception {
    System.out.println("Exclusive");
    IntStream.range(0,10).forEach(System.out::println);
    System.out.println("Inclusive");
    IntStream.rangeClosed(0,10).forEach(System.out::println);

  }

  @Test
  public void rangeIteratingLists() throws Exception {
    List<Person> people = MockData.getPeople();
    /*IntStream.range(0,people.size()).forEach(item->{
              Person person = people.get(item);
              System.out.println(person);
            }
            );*/
    /*people.forEach(p->{
      System.out.println(p);
    });*/
    people.forEach(System.out::println);

  }

  @Test
  public void intStreamIterate() throws Exception {
      IntStream.iterate(0, x -> x + 2)
          .filter(y -> y % 3 == 0)
          .limit(20)
          .forEach(System.out::println);
  }
}
